provider "pg" {
  passphrase = "example"
}

provider "aws" {
  region         = "us-east-2"
}

//resource "aws_db_subnet_group" "qa-wallet-db-2" {
//  name       = "qa-wallet-db-2"
//  subnet_ids = [local.base.vpc.public_subnet_a, local.base.vpc.public_subnet_b]
//}

resource "aws_db_instance" "qa-wallet-db-2" {
  identifier                 = "test-db-2"
  allocated_storage          = 10
  storage_type               = "gp2"
  storage_encrypted          = false
  engine                     = "postgres"
  engine_version             = "12.2"
  instance_class             = "db.t2.micro"
  deletion_protection        = false
  auto_minor_version_upgrade = false
  multi_az                   = false
  publicly_accessible        = true
  username                   = "postgres"
  password                   = "secret"
}