FROM alpine:3.8

ENV TFLINT_VERSION=v0.17.0

WORKDIR /data

RUN apk update && \
    apk add bash ca-certificates openssl unzip wget && \
    cd /tmp && \
    wget https://github.com/terraform-linters/tflint/releases/download/${TFLINT_VERSION}/tflint_linux_amd64.zip && \
    unzip tflint_linux_amd64.zip -d /usr/bin && \
    rm -rf /tmp/* && \
    rm -rf /var/cache/apk/* && \
    rm -rf /var/tmp/*

ENV PATH=$PATH:/root/.local/bin