stages:
  - test
  - package

image: docker:latest

variables:
  IMAGE_TAG: filippovdenis/budgetsrv2:go-docker
  CI_REGISTRY_IMAGE: hub.docker.com/filippovdenis/budgetsrv2:go-docker

test:
  stage: test
  script: cat main.go | grep 'main'

package:
  stage: package
  script: cat main.go build.bat | gzip > package.gz
  artifacts:
    paths:
      - package.gz